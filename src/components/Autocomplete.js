// @flow
import React, { Component, PropTypes } from 'react'
import Block from 'jsxstyle/Block'
import { debounce } from 'lodash'
import Spinner from './Spinner'

const KEY_DOWN_CODE = 40
const KEY_UP_CODE = 38
const ENTER_KEY_CODE = 13
const MAX_RESULTS = 5

export default class Autocomplete extends Component {

	state = {
   		value: '',
    	selectedValue: {},
    	showResults: false,
    	currentSelection: null
  	}

  	static propTypes = {
    	value: PropTypes.string,
    	fetch: PropTypes.func.isRequired,
    	onSelect: PropTypes.func.isRequired,
    	results: PropTypes.arrayOf(PropTypes.shape({
      		label: PropTypes.string.isRequired,
      		value: PropTypes.string.isRequired,
    	})),
    	fetching: PropTypes.bool,
  	}

  	componentWillReceiveProps(nextProps) {
    	if (nextProps.results !== this.props.results) {
      		this.setState({
        		showResults: true,
         		currentSelection: null
      		})
      		document.getElementById('autocomplete-input').focus()
    	}
 	}
 

	handleChange = value => {
		this.setState({
    		value,
       		showResults: false
    	})
    	this.fetch(value)
	}
 
	fetch = value => {
    	if (this.debounced) {
       		this.debounced.cancel()
     	}
     	this.debounced = debounce(() => this.props.fetch(value), 300)
     	this.debounced()
   	}
 
   	handleSelectValue = selectedValue => {
    	this.setState({
       		selectedValue,
       		value: selectedValue.label
     	})
     	this.props.onSelect(selectedValue)
   	}


	handleChangeCurrentSelection = selection => {
    	this.setState({ currentSelection: selection })
   	}
 
   	handleKeyDown = event => {
    	
     	if (event.keyCode === KEY_UP_CODE && this.state.currentSelection > 0) {
       		event.preventDefault()
       		this.setState({ currentSelection: this.state.currentSelection - 1 })
     	} else if (event.keyCode === KEY_DOWN_CODE && this.state.currentSelection < this.props.results.length -1 && this.state.currentSelection < MAX_RESULTS - 1) {
       		event.preventDefault()
       		this.setState({ currentSelection: this.state.currentSelection == null ? 0 : this.state.currentSelection + 1 })
     	} else if (event.keyCode === ENTER_KEY_CODE) {
       		event.preventDefault()
       		this.handleSelectValue(this.props.results[this.state.currentSelection])
     	}
   	}
 
   	render() {
    	return (
       		<div>
         		<Block position="relative">
	           		<input
	           			id="autocomplete-input"
	             		style={{
	               		width: '100%'
	             		}}
	             		className="form-control"
	             		type="text"
	             		onChange={(e) => this.handleChange(e.target.value)}
	             		value={this.state.value}
	             		disabled={this.props.fetching}
	             		onKeyDown={this.handleKeyDown}
	           		/>
		           	{this.props.fetching && (
		            	<Block
		               		position="absolute"
		               		right={5}
		               		top={7}
		             	>
		               		<Spinner />
		             	</Block>
		           	)}
         		</Block>
         		{!this.props.fetching && this.state.selectedValue.label !== this.state.value && this.state.showResults && (
           			<div className="list-group">
             			{this.props.results && this.props.results.map((result, index) => {
			            	return index < MAX_RESULTS ? (
			                	<a
			                		onMouseEnter={() => this.handleChangeCurrentSelection(index)}
			                   		key={result.value}
			                   		href="#"
			                   		className={`list-group-item ${this.state.currentSelection === index && 'active'}`}
			                   		onClick={() => this.handleSelectValue(result)}
			                 	>
			                   	{result.label}
			                 	</a>
			               	) : null
			             })}
           			</div>
         		)}
       		</div>
     	)
   	}
 }