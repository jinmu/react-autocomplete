// @flow
import React, { Component } from 'react'
import Block from 'jsxstyle/Block'
import AutocompleteContainer from './components/AutocompleteContainer'

class App extends Component {
  render() {
    return (
		<Block
       		width={300}
         	margin="auto"
         	marginTop={20}
       >
        	<AutocompleteContainer name="test" />
		</Block>
    )
  }
}

export default App