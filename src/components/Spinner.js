import React, { PropTypes } from 'react'
import Block from 'jsxstyle/Block'

const Spinner = props => (
	<Block textAlign={props.centered ? 'center' : ''}>
    	<i className="fa fa-spinner fa-spin" />
  	</Block>
)

Spinner.propTypes = {
 	centered: PropTypes.bool,
}

Spinner.defaultProps = {
  	centered: false,
}

export default Spinner